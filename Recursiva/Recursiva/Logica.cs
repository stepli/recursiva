﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Recursiva
{
    public class Logica
    {
        public List<Socio> ListaSocios()
        {
            var LSocio = new List<Socio>();
            
            using (StreamReader reader = new StreamReader("C:/Temp/socios.csv", Encoding.GetEncoding(1252)))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    if (!String.IsNullOrWhiteSpace(line))
                    {
                        string[] values = line.Split(';');
                        if (values.Length > 0)
                        {
                            var oSocio = new Socio();
                            oSocio.Nombre = values[0];
                            oSocio.Edad = Convert.ToInt32(values[1]);
                            oSocio.Equipo = values[2];
                            oSocio.EstadoCivil = values[3];
                            oSocio.NivelDeEstudios = values[4];
                            LSocio.Add(oSocio);
                        }
                    }
                }
                reader.Dispose();
                return LSocio;

            }
        }
    }
}
