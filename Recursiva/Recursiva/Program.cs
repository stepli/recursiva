﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recursiva
{
    class Program
    {
        static int tableWidth = 73;

        private static List<Socio> socios = new List<Socio>();
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.GetEncoding(1252);
            var logica = new Logica();
            socios = logica.ListaSocios();

            Punto1();

            Console.WriteLine("\n");
            Console.WriteLine("\n");

            Punto2();

            Console.WriteLine("\n");
            Console.WriteLine("\n");

            Punto3();

            Console.WriteLine("\n");
            Console.WriteLine("\n");

            Punto4();

            Console.ReadLine();

        }

        static void Punto1()
        {
            Console.WriteLine("1) La cantidad de socios son: " + Convert.ToString(socios.Count()));
        }
        static void Punto2()
        {
            Console.WriteLine("2) El listado de 100 primeras personas casadas, con estudios Universitarios, ordenadas de menor a mayor según su edad son:");
            var listadoCasados = socios.Select(s => s).Where(s => s.EstadoCivil is "Casado" && s.NivelDeEstudios is "Universitario").Take(100).OrderByDescending(s => s.Edad).ToList();

            PrintLine();
            PrintRow("Nombre", "Edad", "Equipo");
            PrintLine();

            foreach (var casado in listadoCasados)
            {
                PrintRow((casado.Nombre), Convert.ToString(casado.Edad), casado.Equipo);
            }
            PrintLine();
        }
        static void Punto3()
        {
            Console.WriteLine("3) El listado con los 5 nombres más comunes entre los hinchas de River son:");
            
            var listadoRiverOrdenado = socios.Where(s => s.Equipo is "River").GroupBy(r => r.Nombre)
            .Select(r => new { Nombre = r.First().Nombre, cantidad = r.Count().ToString() })
            .OrderByDescending(r => r.cantidad).Take(5).ToList();

            PrintLine();
            PrintRow("Nombre", "Cantidad");
            PrintLine();

            foreach (var r in listadoRiverOrdenado)
            {
                PrintRow(r.Nombre, r.cantidad);
            }
            PrintLine();
        }
        static void Punto4()
        {
            Console.WriteLine("4) El listado ordenado de mayor a menor según la cantidad de socios, que enumere, junto con cada equipo, el promedio de edad de sus socios, la menor edad registrada y la mayor edad registrada:");

            var listadoEquipos = socios
             .GroupBy(e => e.Equipo)
             .Select(le => new
             {
                 le.First().Equipo,
                 Cantidad = le.Count().ToString(),
                 PromEdad = (le.Sum(s => s.Edad) / le.Count()).ToString(),
                 MaxEdad = le.Max(c => c.Edad).ToString(),
                 MinEdad = le.Min(c => c.Edad).ToString()
             }).OrderByDescending(r => r.Cantidad).ToList();

            tableWidth = 100;
            PrintLine();
            PrintRow("Equipo", "Cantidad", "Promedio de Edad", "Edad Maxima", "Edad Minima");
            PrintLine();

            foreach (var r in listadoEquipos)
            {
                PrintRow(r.Equipo, r.Cantidad, r.PromEdad, r.MaxEdad, r.MinEdad);
            }

            PrintLine();
        }
        static void PrintLine()
        {
            Console.WriteLine(new string('-', tableWidth));
        }

        static void PrintRow(params string[] columns)
        {
            int width = (tableWidth - columns.Length) / columns.Length;
            string row = "|";

            foreach (string column in columns)
            {
                row += AlignCentre(column, width) + "|";
            }

            Console.WriteLine(row);
        }

        static string AlignCentre(string text, int width)
        {
            text = text.Length > width ? text.Substring(0, width - 3) + "..." : text;

            if (string.IsNullOrEmpty(text))
            {
                return new string(' ', width);
            }
            else
            {
                return text.PadRight(width - (width - text.Length) / 2).PadLeft(width);
            }
        }
    }
}
